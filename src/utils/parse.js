// This could probably be written a different way, however for the size of the current app this seemed the most efficient choice.
export const parse = (cards, text) => {
  const splitText = text.split("|");
  let newCards = [];

  const cardAdditions = splitText
    .map(data => {
      if (incorrectInput(data)) {
        return null;
      } else {
        const title = data.split(":")[0];
        const description = data.split(":")[1];
        return newCards.concat({ title, description });
      }
    })
    .flat();

  if (cardAdditions.includes(null)) {
    return { cards, error: true };
  } else {
    return { cards: cards.concat(cardAdditions) };
  }
};

const incorrectInput = data => {
  return !data.includes(":") || data.match(/:/g).length >= 2;
};
