import React, { useState } from "react";
import styled from "styled-components";
import Card from "./Card";
import Field from "./Field";
import { parse } from "../utils/parse";

type CardType = {
  title: string;
  description: string;
};

// I chose to keep all of my state in this top-level component, but would change that later on if need be.
const Main = () => {
  const defaultValues = {
    text: ""
  };

  const defaultCards: any = [];

  const [values, setValues] = useState(defaultValues);
  // If the app were to grow, I would put "cards" and "error" in redux or another state management tool.
  const [cards, setCards] = useState(defaultCards);
  const [error, setError] = useState("");

  const inputHandler = (text: string) => {
    setValues({ text });
  };

  const submitHandler = (text: string) => {
    const result = parse(cards, text);
    if (result.error) {
      setError("* Please include a key value pair between each delimiter (|)");
    } else {
      setCards(result.cards);
      setError("");
      setValues(defaultValues);
    }
  };

  const deleteHandler = (cardIndex: number) => {
    const newCards = [...cards];
    newCards.splice(cardIndex, 1);
    setCards(newCards);
  };

  return (
    <Container>
      <InputSection>
        <Field values={values} inputHandler={inputHandler} />
        <Error>{error}</Error>
        <Button
          onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) =>
            submitHandler(values.text)
          }
        >
          Parse
        </Button>
      </InputSection>
      <Wrapper>
        {cards.map((card: CardType, i: number) => (
          <Card card={card} deleteHandler={deleteHandler} cardIndex={i} />
        ))}
      </Wrapper>
    </Container>
  );
};

export default Main;

const Container = styled.div`
  display: grid;
  grid-gap: 50px;
  grid-template-rows: repeat(auto-fill, minmax(500px, 10% 2% 88%));
  width: 100%;
  padding: 40px 20px;
`;

const InputSection = styled.div`
  max-width: 1000px;
  width: 100%;
  margin: auto;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  color: red;
`;

const Button = styled.button`
  text-transform: uppercase;
  font-size: 1.2em;
  background: #5582c2;
  border-bottom: 1px solid black;
  border-right: 1px solid black;
  padding: 5px 10px;
`;

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(200px, 5fr));
  grid-gap: 30px;
  width: 100%;
  max-width: 1000px;
  margin: auto;
`;

const Error = styled.div`
  margin-left: 20px;
`;
