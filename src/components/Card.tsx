import React from "react";
import styled from "styled-components";

type CardType = {
  title: string;
  description: string;
};

type Props = {
  card: CardType;
  deleteHandler: Function;
  cardIndex: Number;
};

const Card: React.FunctionComponent<Props> = ({
  card,
  deleteHandler,
  cardIndex
}) => {
  return (
    <Container>
      <Header>
        {card.title}
        <Button
          onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
            deleteHandler(cardIndex);
          }}
        >
          <i className="fas fa-times" />
        </Button>
      </Header>
      <Section>{card.description}</Section>
    </Container>
  );
};

export default Card;

const Container = styled.div`
  display: grid;
  grid-template-rows: 25% 75%;
  width: 100%;
  background: #d3d3d3;
  border: solid black 1px;
  transition: 0.3s all ease-in;
`;

const Section = styled.div`
  background: #d3d3d3;
  height: 120px;
  grid-template-columns: 2fr;
  transition: 0.3s background ease-in;
  padding: 20px;
  border-top: solid black 1px;
`;

const Header = styled.div`
  height: 75px;
  font-weight: bold;
  font-size: 1.2em;
  display: flex;
  flex-direction: columns;
  justify-content: space-between;
  align-items: flex-start;
  padding: 15px 20px;
`;

const Button = styled.button`
  background-color: transparent;
  border: none;
  color: red;
  font-size: 1.2em;
`;
