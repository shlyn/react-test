import React from "react";
import styled from "styled-components";

type Props = {
  values: {
    text: string;
  };
  inputHandler: Function;
};

// The wireframe did not include information on the exact sizing of this input, but I tried to scale it as close as possible.
const Field: React.FunctionComponent<Props> = ({ values, inputHandler }) => {
  return (
    <BoxInput
      name="text"
      placeholder="Enter key value pair"
      value={values.text}
      onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
        inputHandler(e.target.value)
      }
    />
  );
};

export default Field;

const BoxInput = styled.input`
  width: 100%;
  font-size: 3em;
`;
