import React from "react";
import Main from "./components/Main";
import { GlobalStyle } from "./styles/GlobalStyle";
import styled from "styled-components";

const App: React.FC = () => {
  return (
    <Container>
      <GlobalStyle />
      <Main />
    </Container>
  );
};

export default App;

const Container = styled.div`
  height: 100%;
  background-image: linear-gradient(
    to left bottom,
    #ffffff,
    #f2f2f2,
    #e4e4e4,
    #d7d7d7,
    #cacaca
  );
`;
